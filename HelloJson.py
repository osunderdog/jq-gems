import json
import jq
from typing import List, Iterator, Dict
import random
import itertools
from argparse import ArgumentParser
from sys import stdout

def getargs() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('--count', default=4, type=int, help="number of records to output.")
    parser.add_argument('--filename', default=None, type=str, help="the name of the output file if not set then stream to stdout")
    parser.add_argument('--recordstream', action='store_true', help="write record wise output rather than list wise.")
    return parser


def random_category_gen(cat_list: List[str]) -> Iterator[str]:
    """
    Generate an infinite list of random categorical values using catList population
    :param cat_list:
    """
    return (random.choices(population=cat_list, k=1).pop() for _ in itertools.count())


def random_value_gen(start_range: float = -1.0, end_range: float = 1.0):
    return (random.uniform(start_range, end_range) for _ in itertools.count())


def test_data_gen(cat: Iterator[str], value: Iterator[float]) -> Iterator[Dict]:
    for i in itertools.count():
        yield {'index': i, 'value': next(value), 'category': next(cat)}


def sample_data(data_iter, size=4):
    return [next(data_iter) for _ in range(size)]

def main():
    parser = getargs()
    conf = parser.parse_args()
    ofh = stdout
    if conf.filename:
        ofh = open(conf.filename, 'w')

    value_iter = random_value_gen()
    cat_list_iter = random_category_gen(['this', 'that', 'these'])
    data_iter = test_data_gen(cat=cat_list_iter, value=value_iter)
    if conf.recordstream:
        for _ in range(conf.count):
            print(json.dumps(next(data_iter)), file=ofh)
    else:
        print(json.dumps(sample_data(data_iter, size=conf.count)), file=ofh)
    ofh.close()

if __name__ == '__main__':
    main()
