import json
import jq
import pathlib

def main():
    filename = 'data/sample_data_01.json'
    fnpath = pathlib.Path(filename)
    with fnpath.open("r") as ifh:
        json_body = json.load(ifh)
    print(json_body)

    jqc = jq.compile("length")
    result = jqc.input(json_body)
    print(result.first())
    print(result.all())

if __name__ == '__main__':
    main()